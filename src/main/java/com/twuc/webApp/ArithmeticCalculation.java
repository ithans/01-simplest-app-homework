package com.twuc.webApp;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ArithmeticCalculation {
    @RequestMapping("/api/tables/plus")
    public String AddCalculation() {
        StringBuffer sb = new StringBuffer();
        int sum = 0;
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; i >= j; j++) {
                sum = i + j;
                if (sum >= 10) {
                    sb.append(i + "+" + j + "=" + sum + " ");
                } else {
                    sb.append(i + "+" + j + "=" + sum + "&nbsp;&nbsp;");
                }
            }
            sb.append("<br />");
        }
        return sb.toString();
    }

    @RequestMapping("/api/tables/multiply")
    public String MultiplyCalculation() {
        StringBuffer sb = new StringBuffer();
        int mult = 0;
        for (int i = 1; i <= 9; i++) {
            for (int j = 1; i >= j; j++) {
                mult = i * j;
                if (mult >= 10) {
                    sb.append(i + "*" + j + "=" + mult + " ");
                } else {
                    sb.append(i + "*" + j + "=" + mult + "&nbsp;&nbsp;");
                }
            }
            sb.append("<br />");
        }
        return sb.toString();
    }
}
