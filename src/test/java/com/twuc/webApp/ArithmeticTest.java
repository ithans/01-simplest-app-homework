package com.twuc.webApp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@SpringBootTest
@AutoConfigureMockMvc
public class ArithmeticTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void plus_should_return_state_200() throws Exception {
        int state = mockMvc.perform(get("/api/tables/plus")).andReturn().getResponse().getStatus();
        Assertions.assertEquals(state, 200);
    }

    @Test
    public void plus_should_return_contentType() throws Exception {
        String contentType = mockMvc.perform(get("/api/tables/plus")).andReturn().getResponse().getContentType();
        Assertions.assertTrue(contentType.contains("text/plain"));
    }

    @Test
    public void mult_should_return_state_200() throws Exception {
        int state = mockMvc.perform(get("/api/tables/multiply")).andReturn().getResponse().getStatus();
        Assertions.assertEquals(state, 200);
    }

    @Test
    public void mult_should_return_contentType() throws Exception {
        String contentType = mockMvc.perform(get("/api/tables/multiply")).andReturn().getResponse().getContentType();
        Assertions.assertTrue(contentType.contains("text/plain"));
    }
}
